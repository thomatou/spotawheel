<?php

namespace App\Models;

class Payment extends SpotaWheel
{
    public $table = 'payments';
    protected $fillable = ['amount', 'user_id'];
}
