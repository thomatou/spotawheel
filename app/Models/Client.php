<?php

namespace App\Models;

class Client extends SpotaWheel
{
    protected $fillable = ['name', 'surname'];
    public $table = 'clients';

    public function payments() : \Illuminate\Database\Eloquent\Relations\hasMany
    {
        return $this->hasMany(Payment::class, 'user_id', $this->id);
    }

    public function latestPayment() : \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Payment::class, 'user_id', $this->id)
                ->orderBy("created_at", "desc");
    }

    public static function getAllWithLatestPayment($params = []) : \Illuminate\Database\Eloquent\Builder
    {
        return self::with(['latestPayment' => function($query) use ($params) {
            if (sizeof($params)) {
                if (isset($params["date_from"])) {
                    $query->where("created_at", ">=", $params["date_from"]);
                }

                if (isset($params["date_to"])) {
                    $query->where("created_at", "<=", $params["date_to"]);
                }
            }
        }]);
    }
}
