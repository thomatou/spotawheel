<?php

namespace App\Console\Commands;

use App\Models\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CreateCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csv:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'creates csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $today = date("Y-m-d");
        $beforeThirtyDays =  date('Y-m-d', strtotime('-30 days', strtotime($today)));

        $content = [
            [
                "name",
                "surname",
                "amount",
                "date"
            ]
        ];

        foreach (Client::getAllWithLatestPayment(["date_from" => $beforeThirtyDays, "date_to" => $today])->get() as $client) {
            $date = "";
            $amount = "";
            if ($client->latestPayment) {
                $date = $client->latestPayment->created_at;
                $amount = $client->latestPayment->amount;
            }

            $content[] = [
                $client->name,
                $client->surname,
                $amount,
                $date
            ];
        }

        $csvContent = "";

        foreach ($content as $item) {
            $csvContent .= join(",", $item)."\n";
        }


        Storage::put(now().".csv", $csvContent);
        return 0;
    }
}
