<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class SpotaWheelController extends Controller
{
    public function index(Request $request) {

        $dateData = [];

        if (isset($request["date_from"])) {
            $this->validate($request,[
                "date_from" => "date"
            ]);
            $dateData["date_from"] = $request["date_from"];
        }

        if (isset($request["date_to"])) {
            $this->validate($request,[
                "date_to" => "date"
            ]);
            $dateData["date_to"] = $request["date_to"];
        }

        if (isset($request["date_from"]) && $request["date_to"]) {
            $this->validate($request,[
                "date_from" => "date|before_or_equal:date_to",
                "date_to" => "date|after_or_equal:date_from"
            ]);
        }

        $clients = [];

        foreach (Client::getAllWithLatestPayment($dateData)->get() as $client) {
            $data = [
                "id" => $client->id,
                "name" => $client->name,
                "surname" => $client->surname,
                "latestPayment" => "no payment",
                "amount" => ""
            ];

            if ($client->latestPayment) {
                $data["latestPayment"] = $client->latestPayment->created_at;
                $data["amount"] = $client->latestPayment->amount;
            }

            $clients[] = $data;
        }

        return  view("index",[
            "clients" => $clients
        ]);
    }
}
