<table>
    <thead style="text-align: left">
    <tr>
        <th>id</th>
        <th>name</th>
        <th>surname</th>
        <th>amount</th>
        <th>lastest payment</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($clients as $client)
        <tr>
            <td>
                {{ $client["id"] }}
            </td>
            <td>
                {{ $client["name"] }}
            </td>
            <td>
                {{ $client["surname"] }}
            </td>
            <td class="text-center">
                {{ $client["amount"] }}
            </td>
            <td class="text-center">
                {{ $client["latestPayment"] }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
